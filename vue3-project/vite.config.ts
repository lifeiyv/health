import { fileURLToPath, URL } from 'node:url';

import autoprefixer from 'autoprefixer';
import pxtorem from 'postcss-pxtorem';
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue()
  ],
  css: {
    postcss: {
      plugins: [
        autoprefixer({
          overrideBrowserslist: [
            "Android 4.1",
            "iOS 7.1",
            "Chrome > 31",
            "ff > 31",
            "ie >= 8",
            "last 10 versions", // 所有主流浏览器最近10版本用
          ],
          grid: true,
        }),
        pxtorem({
          rootValue: 37.5, // 设计稿宽度的1/ 10 例如设计稿按照 1920设计 此处就为192
          propList: ["*"], // 除 border 外所有px 转 rem
          selectorBlackList: [], // 过滤掉.el-开头的class，不进行rem转换
        }),
      ],
    }
  },
  build: {
    terserOptions: {
      compress: {
        drop_console: true, // 关闭所有的 console.log 打印
    },
    },
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    },
    extensions: [".js", ".ts", ".jsx", ".tsx", ".json", ".vue"], // 后缀省略
  }
})
