import store from '@/store'
import { getChatRoomBind } from '@/api/chatApi'
 
 
let heartBeat, //心跳得定时器
    serverHeartBeat, //服务器响应的定时器
    beat_time = 50000, //心跳时间间隔
    reconnectTimer,
    reconnectNum = 3, //重连次数
    reconnectFlag = true, //控制重连，一次一次来
    beat_data = {
        chat:"ping",
  }
let client_id 
let isAccident = 1
export default class WebSocketClient {
  private ws: WebSocket | any;
  private url;
  private message_func;
  constructor(url: string) {
    this.url = url
   this.initWebSocket(url)
   
  }
  initWebSocket(url) {
    this.ws = new WebSocket(url);
    this.ws.onopen = () => {
      //重连之后需要再绑定
      // if (reconnectNum < 3 && store.getters.token.access_token) {
      //   getChatRoomBind(store.state.chat.client_id)
      // }
      // 开始心跳
      console.log('链接成功');
      
      this.heartBeat(1)
      reconnectTimer && clearTimeout(reconnectTimer)
    }; 
 
    this.ws.onmessage = (event) => {
      
      let msg = JSON.parse(event.data)
      if (msg.chat === 'pong') {
        console.log('正常');
        serverHeartBeat && clearTimeout(serverHeartBeat)
      } 
      if (msg.type == 'connect' && reconnectNum < 3) {
        client_id = msg.client_id
        store.commit('setClientId', msg.client_id)
        if (reconnectNum < 3 && store.getters.token.access_token) {
          getChatRoomBind(client_id)
          reconnectNum = 3
        }
      }
      if (msg.chat !== 'pong') {
        if (this.message_func) {
          this.onMessage(this.message_func)
        }
      }
       
    }  
 
    this.ws.onclose = () => {
      console.log('断线,onclose');
      
      if (store.getters.token.access_token && isAccident == 1) {
        this.reconnect()
      }
      this.heartBeat(2)
    };
 
    this.ws.onerror = (error: Event) => {
      if (store.getters.token.access_token) {
        this.reconnect()
      }
      this.heartBeat(2)
 
    };
 
  }
 
  public send(data: any) {
 
    if (this.ws.readyState === WebSocket.OPEN) {
      this.ws.send(JSON.stringify(data));
    } else {
      console.error('WebSocket未连接');
    }
  }
 
  public close(accident = 1) {
    console.log('断线,close');
    this.heartBeat(2)  
    this.ws.close();
    isAccident = accident
    //是否是主动断开的，1意外，0主动
    if (accident == 1) { 
      this.reconnect()
    }
  
  }
 
  
 
 
  // 监听消息
  onMessage(callback: (data: any) => void): void {
    this.ws.onmessage = event => {
 
      this.message_func = callback
       //忽略心跳返回信息
       let msg = JSON.parse(event.data)
       if (msg.chat !== 'pong') {
         callback(event.data);
      }
      if (msg.chat === 'pong') {
        serverHeartBeat && clearTimeout(serverHeartBeat)
      } 
    }
   
        
 
     
    
  }
  onOpen(callback: (data:any) => void): void{
    this.ws?.addEventListener('open', (data:any) => {
 
      callback(data)
    })
  }
 
 
  heartBeat(opa = 1) {
    // 是否开启心跳
    if (opa == 1) {
      heartBeat = setInterval(() => { 
        if (this.ws.readyState === WebSocket.OPEN) {
          console.log('心跳');
          this.send(beat_data)
          serverHeartBeat = setTimeout(() => {
            //3秒内没收到消息，断开重连
            this.close()
            clearInterval(heartBeat)
          }, 3000);
        }  
        //
      }, beat_time)
      
    } else {
      heartBeat && clearInterval(heartBeat)
      serverHeartBeat && clearTimeout(serverHeartBeat)
     }
  }
  
  reconnect() {
    heartBeat && clearInterval(heartBeat)
    serverHeartBeat && clearTimeout(serverHeartBeat)
    if(!reconnectFlag) return
    if (reconnectNum > 0 && reconnectFlag) {
      reconnectTimer = setTimeout(() => {
        console.log('重连',reconnectNum);
        
        this.initWebSocket(this.url)
        reconnectNum -= 1
        reconnectFlag = true
      }, 5000);
      reconnectFlag = false
    } else {
      console.error('websocket error');
    }
  }
  
}