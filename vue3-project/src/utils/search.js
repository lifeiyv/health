// 防抖
export const debounce = (func, delay = 500) => {
  let timer = 0
  return function (...args) {
    if (timer) clearTimeout(timer)
    timer = setTimeout(() => {
      func.apply(this, args)
    }, delay)
  }
}

// 节流
export const throttle = (func, delay = 500) => {
  let timer = 0
  return function (...args) {
    if (timer) return
    timer = setTimeout(() => {
      func.apply(this, args)
      timer = 0
    }, delay)
  }
}
