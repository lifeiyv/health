import axios from "axios";
import { showSuccessToast, showFailToast } from "vant";

// 创建axios实例的方法
export function createAxios(options = {}) {
  return axios.create({ ...options });
}

export const AxiosServer = createAxios({
  baseURL: "https://node.lifeiyu.love",
  headers: {
    Authorization: "AUTH_TOKEN_INSTANCE",
  },
  timeout: 5000,
});

// 刷新Token
async function refreshToken() {
  const res = await AxiosServer.get("/fwc/refresh", {
    params: {
      token: localStorage.getItem("refreshToken"),
    },
  });
  if (res.status == 200) {
    localStorage.setItem("accessToken", res.data.data.accessToken);
    localStorage.setItem("refreshToken", res.data.data.refreshToken);
  }
  return res;
}

// 请求拦截器
AxiosServer.interceptors.request.use((config) => {
  const accessToken = localStorage.getItem("accessToken");
  if (accessToken) {
    config.headers.authorization = "Bearer " + accessToken;
  }
  return config;
});

let refreshing = false;
let queue = []; // 请求队列

// 响应拦截器
AxiosServer.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    let { status, data, config } = error.response;
    if (error.response.data.code == 20000) {
      // showFailToast("请重新登录");
    }
    if (refreshing) {
      return new Promise((resolve) => {
        queue.push({
          config,
          resolve,
        });
      });
    }
    if (status === 401 && !config.url.includes("/fwc/refresh")) {
      refreshing = true;
      const res = await refreshToken(); // 刷新Token
      refreshing = false;
      if (res.status === 200) {
        queue.forEach(({ config, resolve }) => {
          resolve(AxiosServer(config));
        });
        return AxiosServer(config); // 重发请求
      } else {
        alert(data.msg || "登录过期，请重新登录");
        return {};
      }
    } else {
      return error.response;
    }
  }
);
