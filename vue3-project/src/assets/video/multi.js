$(function(){

    //一进页面是知道视频数量的（示例假设有3个视频），我们约定每个video的id规则为demo_video_x，x为1，2，3等视频数或其他唯一标识
    var video_num = 3;
    for(i=1;i<=video_num;i++){
         video_demo(i)  
    }

    function video_demo(vid){
        var time1;
        var t1 = 0;
        
        function aa() {
            t1 += 0.25;
            document.getElementById('aa_'+vid).value = t1;
            console.log('aa_'+vid+'-' + t1);
        }
        console.log('video'+vid)
        var video = document.getElementById("demo_video_"+vid);
        console.log(video)
        video.addEventListener("canplay",function(){
            console.log("canplay"+vid);
        },false)
        //播放结束
        video.addEventListener("ended",function(){
            console.log("ended"+vid);
            window.clearInterval(time1);
            countTime();   //向后台发数据
        },false)
        //暂停
        video.addEventListener("pause",function(){
            console.log("pause"+vid);
            window.clearInterval(time1);
            countTime(); 
        },false)
        //被主动暂停或网络暂停缓冲后重新播放会触发该事件
        //开始播放视频时，设置一个定时器，每250毫秒调用一次aa(),观看时长加0.25秒，
        //定时器需要放在playing事件中，否则无法实现网络缓冲后继续计时的功能
        video.addEventListener("playing",function(){
            console.log("playing"+vid);
            time1 = setInterval(function () {
                aa();
            }, 250)
        },false)
        //当因网络原因导致暂停时会触发该事件
        video.addEventListener("waiting",function(){
            console.log('waiting'+vid);
            window.clearInterval(time1);
            countTime();   //向后台发数据
        },false)
    
         //向后台发数据
         function countTime(){
            console.log('countTime'+vid,document.getElementById('aa_'+vid).value);
        }
         //直接关闭页面，并向后台发送数据
         if(window.addEventListener){
            window.addEventListener("beforeunload",countTime,false);
        }else{
            window.attachEvent("onbeforeunload",countTime);
        }
    }
    

})