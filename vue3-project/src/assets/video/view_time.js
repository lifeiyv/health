$(document).ready(function () {
    var options = {
    };
    var player = videojs('demo-video', options, function onPlayerReady() {
        var time1;
        var t1 = 0;
        function aa() {
            t1 += 0.25;
            document.getElementById('aa').value = t1;
            console.log('aa-' + t1);
        }
        //开始播放视频时
        this.on('play', function () {
            console.log('开始播放'); 
        });
        //结束和暂时时清除定时器，并向后台发送数据
        this.on('ended', function () {
            console.log('结束播放');
            window.clearInterval(time1);
            countTime();   //向后台发数据
        });
        this.on('pause', function () {
            console.log('暂停播放');
            window.clearInterval(time1);
            countTime();  //向后台发数据
        });
		//被主动暂停或网络暂停缓冲后重新播放会触发该事件
		//开始播放视频时，设置一个定时器，每250毫秒调用一次aa(),观看时长加0.25秒，
		//定时器需要放在playing事件中，否则无法实现网络缓冲后继续计时的功能
		this.on("playing",function(){
			console.log('开始播放');
			time1 = setInterval(function () {
                aa();
            }, 250)
		}),
		//当因网络原因导致暂停时会触发该事件
		this.on("waiting",function(){
			window.clearInterval(time1);
           		 countTime();   //向后台发数据
			 console.log('waiting');
		})
    });
    //直接关闭页面，并向后台发送数据
    if(window.addEventListener){
        window.addEventListener("beforeunload",countTime,false);
    }else{
        window.attachEvent("onbeforeunload",countTime);
    }
	
	function countTime(){
		console.log('countTime',document.getElementById('aa').value);
	}
})

