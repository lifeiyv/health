import './assets/main.css'

import "amfe-flexible/index.js";
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate';

import App from './App.vue'
import router from './router'
import Vant from 'vant'
import 'vant/lib/index.css';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import VueCropper from "vue-cropper"
import 'vue-cropper/dist/index.css'
import 'element-plus/theme-chalk/dark/css-vars.css'


// 创建Pinia实例并使用插件
const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);


const app = createApp(App)

// 在全局上注册插件 VirScroll
import VirScroll from './plugins'
app.use(VirScroll)

// 全局防抖节流函数
import{debounce,
} from './utils/hook.js'
app.config.globalProperties.$debounce = debounce;
// app.config.globalProperties.$throttle = throttle;

app.use(pinia)
app.use(router)
app.use(Vant)
app.use(ElementPlus)
app.use(VueCropper)

app.mount('#app')
