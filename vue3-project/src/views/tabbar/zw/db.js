// 创建 IndexedDB 数据库
function openDB(dbName, version = 1) {
  return new Promise((resolve, reject) => {
    const indexedDB =
      window.indexedDB ||
      window.mozIndexedDB ||
      window.webkitIndexedDB ||
      window.msIndexedDB;

    let db;
    const request = indexedDB.open(dbName, version);

    request.onsuccess = function (event) {
      db = event.target.result;
      // console.log("数据库打开成功");
      resolve(db);
    };

    request.onerror = function (event) {
      // console.log("数据库打开报错");
      reject(event);
    };

    request.onupgradeneeded = function (event) {
      console.log("onupgradeneeded");

      db = event.target.result; // 数据库对象

      // 创建存储库
      const objectStore = db.createObjectStore("users", {
        autoIncrement: true, // 自增主键
      });

      // 创建索引
      objectStore.createIndex("name", "name", { unique: false });
      objectStore.createIndex("img", "img", { unique: false });
      objectStore.createIndex("image", "image", { unique: false });
      objectStore.createIndex("content", "content", { unique: false });
      objectStore.createIndex("time", "time", { unique: false });
      objectStore.createIndex("flag", "flag", { unique: false });
      objectStore.createIndex("disease", "disease", { unique: false });

    };
  });
}

// 插入数据
function addData(db, storeName, data) {
  return new Promise((resolve, reject) => {
    const transaction = db.transaction(storeName, "readwrite");
    const store = transaction.objectStore(storeName);

    for (let i = 0; i < data.length; i++) {
      store.put(data[i]);
    }

    transaction.oncomplete = function (event) {
      // console.log("数据写入成功");
      resolve();
    };

    transaction.onerror = function (event) {
      // console.log("数据写入失败");
      reject(event);
    };
  });
}
export { addData, openDB }