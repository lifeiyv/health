import VirScroll from './VirScroll.vue'
const plugin={
    install(Vue){
        Vue.component('VirScroll',VirScroll)
    }
}
export default plugin