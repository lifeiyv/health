import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import { showToast } from "vant";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "homeview",
      component: HomeView,
      redirect: "/login",
      children: [
        {
          // 首页
          path: "/home",
          name: "home",
          component: () => import("../views/tabbar/jry/Home.vue"),
        },
        {
          // 科普页面
          path: "/popular",
          name: "popular",
          component: () => import("../views/tabbar/zw/Popular.vue"),
        },
        {
          // 问诊页面
          path: "/Consultation",
          name: "Consultation",
          component: () => import("../views/tabbar/lmy/Consultation.vue"),
        },
        {
          // 我的页面
          path: "/my",
          name: "my",
          component: () => import("../views/tabbar/wrj/Mine.vue"),
          meta: { requiresAuth: true },
        },
        {
          // 登录页面
          path: "/login",
          name: "login",
          component: () => import("../views/tabbar/fwc/Login.vue"),
          meta: { requiresAuth: true },
        },
      ],
    },
    {
      // 注册页面
      path: "/register",
      name: "register",
      component: () => import("../components/Login/Register/register.vue"),
      meta: { requiresAuth: true }
    }, {
      // 忘记密码页面
      path: "/forget",
      name: "forget",
      component: () => import("../components/Login/ForgotPassword/forget.vue"),
      meta: { requiresAuth: true }
    },
    {
      // 忘记密码页面验证码
      path: "/forgetp",
      name: "forgetp",
      component: () => import("../components/Login/ForgotPassword/forgetp.vue"),
      meta: { requiresAuth: true }
    },
    {
      // 忘记密码页面重置密码
      path: "/resetp",
      name: "resetp",
      component: () => import("../components/Login/ForgotPassword/resetp.vue"),
      meta: { requiresAuth: true }
    },
    {
      // 搜索页面
      path: "/search1",
      name: "search1",
      component: () => import("../views/tabbar/jry/Search.vue"),
      meta: { requiresAuth: true },
    },
    {
      // 医生列表页面
      path: "/doclsit",
      name: "doclsit",
      component: () => import("../views/tabbar/jry/DoctorList.vue"),
      meta: { requiresAuth: true },
    },
    {
      // 搜索结果页面
      path: "/searchResult",
      name: "searchResult",
      component: () => import("../views/tabbar/jry/Search_resule.vue"),
      meta: { requiresAuth: true },
    },
    {
      path: "/404",
      component: () => import("../views/404/index.vue"),
      name: "404",
    },
    {
      // 科普搜索页面
      path: "/search",
      name: "search",
      component: () => import("../views/tabbar/zw/Search.vue"),
    },
    {
      // 科普详情页面
      path: "/details/:id",
      name: "details",
      component: () => import("../views/tabbar/zw/Details.vue"),
    },

    //王瑞锦-编辑资料
    {
      path: "/edituser",
      component: () => import("../views/tabbar/wrj/Edituser.vue"),
    },
    //王瑞锦-我的医生
    {
      path: "/follow",
      component: () => import("../views/tabbar/wrj/Follow.vue"),
    },
    //王瑞锦-我的收藏
    {
      path: "/collect",
      component: () => import("../views/tabbar/wrj/Collect.vue"),
    },
    //王瑞锦-我的处方
    {
      path: "/prescription",
      component: () => import("../views/tabbar/wrj/Prescription.vue"),
    },
    //王瑞锦-患者管理
    {
      path: "/patient",
      component: () => import("../views/tabbar/wrj/Patient.vue"),
    },
    //王瑞锦-我的优惠券
    {
      path: "/coupon",
      component: () => import("../views/tabbar/wrj/Coupon.vue"),
    },
    //王瑞锦-联系客服
    {
      path: "/service",
      component: () => import("../views/tabbar/wrj/Service.vue"),
    },
    //王瑞锦-设置
    {
      path: "/settings",
      component: () => import("../views/tabbar/wrj/Settings.vue"),
    },
    //王瑞锦-关于我们
    {
      path: "/aboutus",
      component: () => import("../views/tabbar/wrj/Aboutus.vue"),
    },
    //王瑞锦-服务条款
    {
      path: "/terms",
      component: () => import("../views/tabbar/wrj/Terms.vue"),
    },
    //王瑞锦-隐私政策
    {
      path: "/policy",
      component: () => import("../views/tabbar/wrj/Policy.vue"),
    },
    //王瑞锦-注销账号
    {
      path: "/cancel",
      component: () => import("../views/tabbar/wrj/Cancel.vue"),
    },
    //王瑞锦-注销账号
    {
      path: "/reason",
      component: () => import("../views/tabbar/wrj/Reason.vue"),
    },
    //王瑞锦-健康百科
    {
      path: "/health",
      component: () => import("../views/tabbar/wrj/Health.vue"),
    },
    //王瑞锦-文章详情
    {
      path: "/details",
      component: () => import("../views/tabbar/wrj/Details.vue"),
    },
    //王瑞锦-添加患者
    {
      path: "/insert",
      component: () => import("../views/tabbar/wrj/Insert.vue"),
    },
    //王瑞锦-药品信息
    {
      path: "/drug",
      component: () => import("../views/tabbar/wrj/Drug.vue"),
    },
    //王瑞锦-编辑患者信息
    {
      path: "/editpatient",
      component: () => import("../views/tabbar/wrj/Editpatient.vue"),
    },
    {
      path: "/uploadImg",
      component: () => import("../views/tabbar/wrj/UploadImg.vue"),
    },
    //王瑞锦-订单支付
    {
      path:"/order",
      component:()=>import("../views/tabbar/wrj/Order.vue")
    },
    {
      path:"/tailor",
      component:()=>import("../views/tabbar/wrj/Tailor.vue")
    },
    //找医生
    {
      path: "/seekdoctor",
      component: () => import("../views/tabbar/lfy/seekdoctor.vue"),
    },
    //健康百科
    {
      path: "/wikipedia",
      component: () => import("../views/tabbar/lfy/wikipedia.vue"),
    },
    //健康百科详情
    {
      path: "/wikipediadetails/:id",
      name: "wikipediadetails",
      component: () => import("../views/tabbar/lfy/wikipediadetails.vue"),
    },
    //医生详情
    {
      path: "/doctordetail/:id",
      component: () => import("../views/tabbar/lfy/doctordetail.vue"),
    },
    //图文问诊
    {
      path: "/imgConsultation/:id",
      component: () => import("../views/tabbar/lfy/imgConsultation.vue"),
    },

  ],
});

export default router;
