// 公共暴露区域

// Wrj 文章接口引入处
export { addPatient, getPatientList, editPatient, delPatient, addUser, getUserList, editUser, delUser } from "./Wrj.js";
// Zw接口引入处

export {gethealthy,getDetidsId,getDetstatus,getcomment,getAddcomment } from "./Zw.js";
// Lfy接口引入处
export {} from "./Lfy.js";

// Jry接口引入处
export { getdoctor} from "./Jry.js";

// Fwc接口引入处
export {
  login,
  getUserInfo,
  captcha,
  register,
  resetPassword,
  validateCode,
  confirmResetPassword,
  getCode,
} from "./Fwc.js";

// Lmy接口引入处
export {} from "./Lmy.js";
