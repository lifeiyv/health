import { AxiosServer } from "../utils/index.js";

// 登录
export const login = (data) => {
  return AxiosServer.post("/fwc/login", data);
};

// 获取用户信息
export const getUserInfo = () => {
  return AxiosServer.post("/fwc/getUserInfo");
};

// captcha
export const captcha = (props) => {
  return AxiosServer.post("/fwc/captcha", props);
};

// 注册
export const register = (props) => {
  return AxiosServer.post("/fwc/register", props);
};

// 重置密码
export const resetPassword = (props) => {
  return AxiosServer.post("/fwc/resetPassword", props);
};

// 验证验证码
export const validateCode = (props) => {
  return AxiosServer.post("/fwc/validateCode", props);
};

// 确认重置密码
export const confirmResetPassword = (props) => {
  return AxiosServer.post("/fwc/confirmResetPassword", props);
};

// 获取验证码
export const getCode = (props) => {
  return AxiosServer.post("/fwc/sendCode", props);
};
