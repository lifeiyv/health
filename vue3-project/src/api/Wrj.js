import { AxiosServer } from "../utils/index.js";

//添加患者信息
export const addPatient=async(params)=>{
    return await AxiosServer.post("/wrj/addpatient",params)
}
//获取所有患者信息
export const getPatientList=async(id)=>{
    let {data:{patientList}}=await AxiosServer.get("/wrj/getpatient",{
        params:{_id:id}
    })
    return patientList
}

//更新患者信息
export const editPatient=async(id,params)=>{
    return await AxiosServer.put(`/wrj/editpatient/${id}`,params)
}
//删除患者
export const delPatient=async(id)=>{
    return await AxiosServer.delete(`/wrj/delpatient/${id}`)
}


//添加用户
export const addUser=async(params)=>{
    return await AxiosServer.post("/wrj/adduser",params)
}
//获取用户数据
export const getUserList=async(id)=>{
    let {data:{userList}}=await AxiosServer.get("/wrj/getuser",{
        params:{_id:id}
    })
    return userList
}
//更新用户
export const editUser=async(id,params)=>{
    return await AxiosServer.put(`/wrj/edituser/${id}`,params)
}
//注销用户
export const delUser=async(id)=>{
    return await AxiosServer.delete(`/wrj/deluser/${id}`)
}
