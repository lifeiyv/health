import { AxiosServer } from "../utils/index.js";
//分类接口
export const gethealthy = async (page, limit) => await AxiosServer.get(`/zw/healthy?page=${page}&limit=${limit}`)

// 课程详情接口
export const getDetidsId = async (id) => AxiosServer.get(`/zw/detids/${id}`);
//关注状态修改接口
export const getDetstatus = async (obj) => AxiosServer.post("/zw/detstatus", obj);
//评论展示接口
export const getcomment = async () => await AxiosServer.get('/zw/comment')
//添加评论
export const getAddcomment = async (obj) => await AxiosServer.post('/zw/addcomment', obj)


