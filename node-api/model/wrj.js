const { Schema } = require("mongoose")
let mongoose = require("./db")

module.exports = {};
//患者列表
const patientSchema = new mongoose.Schema({
    name: String,
    idNumber: String,
    gender: String,
    birthday: String,
    weight: String,
    marital: String,
    allergic: String,
    disease: String,
    liver: String,
    renal: String,
    price: Number,
    create_time: Date
})
const patientModel = mongoose.model("patient", patientSchema, "patient")


//用户信息
const userSchema = new mongoose.Schema({
    avater: String,
    username: String,
    gender: String,
    birthday: String,
})
const userModel = mongoose.model("user", userSchema, "user")

module.exports = {
    patientModel,
    userModel
};
