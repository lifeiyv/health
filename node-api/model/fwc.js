const { Schema } = require("mongoose");
const mongoose = require("./db");
const { v4: uuid } = require("uuid");

// 定义 User 集合的 Schema
const UserSchema = new Schema({
  account: { type: String, required: true },
  password: { type: String },
  mailbox: { type: String },
  resetCode: { type: String, default: null }, // 重置密码验证码
  resetCodeExpires: { type: Date, default: null }, // 验证码过期时间
  state: { type: Boolean, default: true }, // 用户状态，true表示账号密码登录，false表示手机验证登录
});

const UserModel = mongoose.model("User", UserSchema);

const PUserSchema = new Schema({
  account: { type: String, required: true },
  resetCode: { type: String, default: null }, // 重置密码验证码
  resetCodeExpires: { type: Date, default: null }, // 验证码过期时间
  state: { type: Boolean, default: false }, // 用户状态，true表示账号密码登录，false表示手机验证登录
});

const PUserModel = mongoose.model("PUser", PUserSchema);

module.exports = { UserModel, PUserModel };
