const { Schema } = require("mongoose")
let mongoose = require("./db")

const healthySchema = new mongoose.Schema({
    name: String,
    img: String,//头像
    image: Array,//图片数组
    disease: String,//疾病
    content: String,
    time: Date,
    flag: Boolean,
    type: String
});
const healthyModel = mongoose.model("healthy", healthySchema, 'healthy');

const commentSchema = new mongoose.Schema({
    name:String,
    img:String,
    flag:Boolean,
    content:String

});
const commentModel = mongoose.model("comment", commentSchema, 'comment');
module.exports = { healthyModel,commentModel };


