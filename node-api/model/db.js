const mongoose = require('mongoose')

mongoose.connect('mongodb://node:lfy116530@47.120.73.107:27017/node?authSource=node')

const conn = mongoose.connection;

conn.on('open', () => {
    console.log('数据库连接成功');
})
conn.on('error', () => {
    console.log('数据库连接失败');
})

module.exports = mongoose