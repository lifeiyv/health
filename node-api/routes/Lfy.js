var express = require("express");
var router = express.Router();
var { diseaseModel } = require("../model/lfy");
var {healthyModel}=require("../model/zw")

router.get("/disease", async (req, res) => {
  let data = await diseaseModel.find()
  res.send({
    code: 200,
    msg: "列表展示成功",
    data,
  });
});
router.post('/details',async(req,res)=>{
    let id=req.body.id
    const data=await diseaseModel.find({_id:id})
    res.send({
        data
    })
})
router.post('/doctordetail',async(req,res)=>{
  let id=req.body.id
  const data=await healthyModel.find({_id:id})
  res.send({
    data
  })
})
router.post("/search",async(req,res)=>{// 接收关键字
  const { key }= req.body;
  console.log(key);
//当为空时返回空列表，前端判断显示当季热门列表
  if(key ==""){
    res.send({
          data: [],
    });
} else {
//进行模糊匹配，返回匹配列表
const data= await healthyModel.find({}).lean();
const filterData = data.filter((item)=> item.name.includes(key));
res.send({data: filterData
});
}});
module.exports = router;
