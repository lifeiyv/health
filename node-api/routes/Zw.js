var express = require("express");
var router = express.Router();

var { healthyModel, commentModel } = require("../model/zw");
//分类数据
router.get("/healthy", async (req, res) => {
    const page = parseInt(req.query.page) || 1;
    const limit = parseInt(req.query.limit) || 5;
    const skipAmount = (page - 1) * limit;
    let data = await healthyModel.find().skip(skipAmount).limit(limit);
    res.send({
        code: 200,
        msg: "列表展示成功",
        data,
        page,
        limit,
    });
});
//分类详情
router.get("/detids/:id", async (req, res) => {
    const ids = req.params.id;
    const data = await healthyModel.find({ _id: ids });
    res.send({
        code: 200,
        msg: "列表展示成功",
        data
    });
});


//评论内容
router.get("/comment", async (req, res) => {
    let data = await commentModel.find();
    res.send({
        code: 200,
        msg: "列表展示成功",
        data,
    });
});
//添加评论
//添加评论
router.post("/addcomment", async (req, res) => {
    let body = req.body;
    const data = await commentModel.create(body);
    res.send({
        code: 200,
        msg: "ok",
        data
    });
})


module.exports = router;
