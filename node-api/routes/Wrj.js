var express = require("express");
var router = express.Router();
var { patientModel, userModel } = require("../model/wrj")
var mutiparty = require("multiparty")

//图片上传
router.post("/uploadImg", (req, res) => {
    console.log(req.body);
    let form = new mutiparty.Form()
    form.uploadDir = "upload"
    form.parse(req, (err, a, imgData) => {
        res.send({
            imgPath: "http://localhost:3008/" + imgData.file[0].path
        })
    })
})

//用户
router.post("/adduser", (req, res) => {
    userModel.create(req.body)
    res.send({ code: 200 })
})
router.get("/getuser", async (req, res) => {
    let { _id } = req.query
    let userList = []
    if (_id) {
        userList = await userModel.find()
        res.send({ userList })
    } else {
        userList = await userModel.find()
        res.send({ userList })
    }
})
router.put("/edituser/:_id", async (req, res) => {
    let id = req.params._id
    let body = req.body
    await userModel.findByIdAndUpdate(id, body)
    res.send({ code: 200 })
})
router.delete("/deluser/:_id", async (req, res) => {
    let id = req.params._id
    await userModel.deleteOne({ _id: id })
    res.send({ code: 200 })
})

//患者信息
router.post("/addpatient", (req, res) => {
    patientModel.create(req.body.addValue )
    res.send({ code: 200 })
})
router.get("/getpatient", async (req, res) => {
    let { _id } = req.query
    let patientList = []
    if (_id) {
        patientList = await patientModel.find({ _id: _id })
        res.send({ patientList })
    } else {
        patientList = await patientModel.find()
        res.send({ patientList })
    }
})
router.put("/editpatient/:_id", async (req, res) => {
    let id = req.params._id
    let body = req.body
    await patientModel.findByIdAndUpdate(id, body)
    res.send({ code: 200 })
})
router.delete("/delpatient/:_id", async (req, res) => {
    let id = req.params._id
    await patientModel.deleteOne({ _id: id })
    res.send({ code: 200 })
})

module.exports = router;