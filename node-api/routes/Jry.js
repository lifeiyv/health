var express = require("express");
var router = express.Router();
var {healthyModel}=require('../model/zw')
router.get('/getlist',async(req,res)=>{
    let data=await healthyModel.find()
    res.send({
        data
    })
})
module.exports = router;