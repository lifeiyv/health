var express = require("express");
var router = express.Router();
const Core = require("@alicloud/pop-core"); // 引入阿里云 SDK
const { UserModel, PUserModel } = require("../model/fwc");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
// const tencentcloud = require("tencentcloud-sdk-nodejs-captcha");
const nodemailer = require("nodemailer");

// 异步函数，检查用户账号是否存在
async function checkAccountExistence(account) {
  const user = await UserModel.findOne({ account });
  return !!user;
}

// 注册
router.post("/register", async function (req, res, next) {
  try {
    const account = req.body.account.trim(); // 去除前后空白字符
    const password = req.body.password;
    const mailbox = req.body.mailbox;
    // 确保账号既不为null也不为空字符串
    if (!account) {
      return res.send({
        code: 4,
        msg: "账号不能为空",
      });
    }
    // 检查账号是否存在
    if (await checkAccountExistence(account)) {
      return res.send({
        code: 5,
        msg: "该账号已存在",
      });
    }
    const hashedPassword = await bcrypt.hash(password, 10); // 使用bcrypt进行密码哈希
    // 确保account字段在UserModel模型中不会被设置为null
    const user = new UserModel({
      account: account || "default_account_value_if_needed", // 或者直接抛出错误，取决于业务逻辑
      password: hashedPassword,
      mailbox,
      state: true,
    });
    await user.save();
    res.send({
      code: 2,
      msg: "注册成功",
    });
  } catch (err) {
    console.error(err);
    res.send({
      code: -1,
      msg: "服务器内部错误",
    });
  }
});

// 登录
router.post("/login", async function (req, res, next) {
  const account = req.body.account;
  const password = req.body.password;

  const user = await UserModel.findOne({ account });

  if (!user) {
    return res.json({
      code: 20000,
      msg: "用户不存在",
    });
  }

  const isPasswordMatch = await bcrypt.compare(password, user.password);
  if (!isPasswordMatch) {
    return res.json({
      code: 20000,
      msg: "密码错误",
    });
  }

  const userInfo = {
    account,
  };
  const accessToken = jwt.sign(userInfo, "abcdefg", {
    expiresIn: "0.5h",
  });
  const refreshToken = jwt.sign(userInfo, "abcdefg", {
    expiresIn: "7d",
  });

  res.json({
    code: 10000,
    data: {
      userInfo,
      accessToken,
      refreshToken,
    },
  });
});

// 获取用户信息
router.post("/getUserInfo", (req, res) => {
  res.json({
    code: 10000,
    data: req.auth,
  });
});

// 刷新Token
router.get("/refresh", (req, res) => {
  console.log("开始刷新token");
  const token = req.query.token;
  let account = "";
  try {
    const userInfo = jwt.verify(token, "abcdefg");
    account = userInfo.account;
  } catch (error) {
    return res.status(401).json({
      code: 20000,
      msg: "token 失效，请重新登录",
    });
  }
  const userInfo = {
    account,
  };
  const accessToken = jwt.sign(userInfo, "abcdefg", {
    expiresIn: "0.5h",
  });
  const refreshToken = jwt.sign(userInfo, "abcdefg", {
    expiresIn: "7d",
  });
  res.json({
    code: 10000,
    data: {
      accessToken,
      refreshToken,
    },
  });
});

// router.post("/captcha", async (req, res) => {
//   const { Ticket, UserIp, Randstr } = req.body;
//   const CaptchaClient = tencentcloud.captcha.v20190722.Client;

//   // 实例化一个认证对象，入参需要传入腾讯云账户 SecretId 和 SecretKey，此处还需注意密钥对的保密
//   // 代码泄露可能会导致 SecretId 和 SecretKey 泄露，并威胁账号下所有资源的安全性。以下代码示例仅供参考，建议采用更安全的方式来使用密钥，请参见：https://cloud.tencent.com/document/product/1278/85305
//   // 密钥可前往官网控制台 https://console.cloud.tencent.com/cam/capi 进行获取
//   const clientConfig = {
//     Action: "DescribeCaptchaResult",
//     Version: "2019-07-22",
//     credential: {
//       secretId: "AKIDXBxsknnu8GGRUJuKXVLG9uTxV1cO7aEq",
//       secretKey: "AKIDXBxsknnu8GGRUJuKXVLG9uTxV1cO7aEq",
//     },
//     profile: {
//       httpProfile: {
//         endpoint: "captcha.tencentcloudapi.com",
//       },
//     },
//   };

//   // 实例化要请求产品的client对象,clientProfile是可选的
//   const client = new CaptchaClient(clientConfig);
//   const params = {
//     // 固定填值：9。可在控制台配置不同验证码类型。
//     CaptchaType: 9,
//     // 前端回调函数返回的用户验证票据
//     Ticket,
//     // 业务侧获取到的验证码使用者的外网IP
//     UserIp,
//     // 前端回调函数返回的随机字符串
//     Randstr,
//     // 验证码应用ID。登录 验证码控制台，在验证列表的【密钥】列，即可查看到CaptchaAppId。
//     CaptchaAppId: 195409423,
//     // 验证码应用密钥。登录 验证码控制台，在验证列表的【密钥】列，即可查看到AppSecretKey。AppSecretKey属于服务器端校验验证码票据的密钥，请妥善保密，请勿泄露给第三方。
//     AppSecretKey: "NvaBr4dzi0iDR2ikYTVcyVKvf",
//   };
//   try {
//     const data = await client.DescribeCaptchaResult(params);
//     console.log("captcha", data);
//     res.send({
//       code: 0,
//       msg: "验证成功",
//       data,
//     });
//   } catch (error) {
//     console.error("error", error);
//     res.send({
//       code: 1,
//       msg: "验证失败",
//       data: error,
//     });
//   }
// });

// 发送验证码邮件
router.post("/resetPassword", async (req, res) => {
  const { account, mailbox } = req.body;

  const user = await UserModel.findOne({ account });

  if (!user) {
    return res.json({
      code: 20000,
      msg: "用户不存在",
    });
  }

  if (user.mailbox !== mailbox) {
    return res.json({
      code: 20000,
      msg: "邮箱错误",
    });
  }

  // 这里发送验证码
  // 生成一个6位随机数
  const code = Math.floor(Math.random() * 1000000) + 1;
  // 设置验证码1分钟后过期
  const expiration = new Date();
  expiration.setMinutes(expiration.getMinutes() + 1);

  // 存储验证码和过期时间到用户记录
  user.resetCode = code.toString();
  user.resetCodeExpires = expiration;
  await user.save();

  // 创建一个SMTP传输器对象
  const transporter = nodemailer.createTransport({
    host: "smtp.163.com", // SMTP服务器地址，这里以163邮箱为例
    port: 465, // SMTP服务器端口，SSL连接通常为465
    secure: true, // 使用SSL加密连接
    auth: {
      user: "18803192632@163.com", // 发件人的邮箱地址
      pass: "KXVVMNVGZXNXGITA", // 发件人邮箱的密码或授权码
    },
  });

  // 设置邮件内容
  const mailOptions = {
    from: '"医疗demo" <18803192632@163.com>', // 发件人信息
    to: mailbox, // 收件人邮箱地址
    subject: "重置密码", // 邮件标题
    text: "邮件正文（文本格式）", // 纯文本内容
    html: `
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>验证码邮件</title>
            <style>
                body {font-family: Arial, sans-serif; margin: 0; padding: 0;}
                .email-container {max-width: 600px; margin: 0 auto; padding: 20px; background-color: #f9f9f9; border-radius: 5px;}
                h1 {color: #333; font-size: 24px; margin-bottom: 10px;}
                p {color: #555; font-size: 16px; line-height: 1.5;}
                .code-box {background-color: #fff; border: 1px solid #ddd; border-radius: 5px; padding: 10px; text-align: center; margin-top: 20px;}
                .code {font-size: 28px; color: #007BFF;}
                .note {margin-top: 10px; color: #777;}
                .footer {text-align: center; color: #999; margin-top: 40px;}
            </style>
        </head>
        <body>
            <div class="email-container">
                <h1>欢迎使用我们的服务！</h1>
                <p>您好，您正在进行安全操作，请查收以下验证码完成验证。</p>
                <div class="code-box">
                    <span class="code">${code}</span>
                </div>
                <p class="note">此验证码有效期为1分钟，请尽快使用。</p>
                <p class="footer">如果这不是您发起的操作，请忽略此邮件，以保障账户安全。</p>
            </div>
        </body>
        </html>
    `, // HTML内容
  };

  // 发送邮件
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    console.log("邮件发送成功:", info.response);
  });

  res.send({
    code: 2,
    msg: "验证码已发送至您的邮箱",
  });
});

// 验证验证码
router.post("/validateCode", async (req, res) => {
  const { account, code, state } = req.body;

  let user = null;

  if (state == true) {
    user = await UserModel.findOne({ account });
  } else {
    user = await PUserModel.findOne({ account });
  }

  if (!user) {
    return res.json({
      code: 20000,
      msg: "用户不存在",
    });
  }

  if (
    user.resetCode !== code.toString() ||
    user.resetCodeExpires < new Date()
  ) {
    return res.json({
      code: 20000,
      msg: "验证码错误或已过期",
    });
  }

  // 验证码正确且未过期，可以进行下一步操作，比如重置密码
  // 清除验证码和过期时间，防止重复使用
  user.resetCode = null;
  user.resetCodeExpires = null;
  await user.save();

  res.send({
    code: 2,
    msg: "验证码验证成功",
  });
});

// 重置密码
router.post("/confirmResetPassword", async (req, res) => {
  const { account, newPassword } = req.body;
  try {
    const user = await UserModel.findOne({ account });
    console.log("user", user);

    if (!user) {
      return res.json({
        code: 20000,
        msg: "用户不存在",
      });
    }

    // 使用bcrypt对新密码进行哈希处理
    const hashedPassword = await bcrypt.hash(newPassword, 10);
    console.log("hashedPassword", hashedPassword);

    // 更新用户密码
    user.password = hashedPassword;
    // 清除验证码和过期时间，避免重复使用
    user.resetCode = null;
    user.resetCodeExpires = null;

    await user.save();

    res.send({
      code: 2,
      msg: "密码重置成功，请使用新密码登录",
    });
  } catch (err) {
    console.error(err);
    res.send({
      code: -1,
      msg: "服务器内部错误",
    });
  }
});

// 发送短信验证码
const client = new Core({
  // 创建 Client 对象
  accessKeyId: "LTAI5tJRyyroB5FdWwVne8z3", // 替换成自己的 AccessKey ID
  accessKeySecret: "nQKs1glCZhBnFS1o5KSzQ0icZuWFhN", // 替换成自己的 AccessKey Secret
  endpoint: "https://dysmsapi.aliyuncs.com", // API 访问入口，根据实际情况修改
  apiVersion: "2017-05-25", // API 版本号，根据实际情况修改
});

// 生成随机验证码
function generateCode() {
  const code = Math.floor(Math.random() * 1000000) + 1;
  return code.toString();
}

// 发送短信验证码
router.post("/sendCode", async (req, res) => {
  const phoneNumber = req.body.phoneNumber; // 获取手机号码
  const code = generateCode(); // 生成验证码

  const params = {
    RegionId: "cn-beijing", // 短信服务所在区域，可以参考阿里云文档
    PhoneNumbers: phoneNumber, // 目标手机号码
    SignName: "医疗demo", // 短信签名名称，需先在阿里云控制台中申请审核通过
    TemplateCode: "SMS_465906703", // 短信模板 CODE，需先在阿里云控制台中申请审核通过
    TemplateParam: JSON.stringify({
      // 短信模板参数，为 JSON 字符串格式
      code: code, // 模板中的变量名和对应的值
    }),
  };

  const requestOption = {
    // 设置请求超时时间等选项
    method: "POST",
    timeout: 5000,
  };
  // 调用 SendSms 方法发送短信
  client.request("SendSms", params, requestOption).then(
    (result) => {
      console.log(result); // 打印发送结果

      res.status(200).json({
        // 返回状态码和生成的验证码
        message: "验证码已发送，请注意查收！",
        code: code,
      });
    },
    (ex) => {
      console.log(ex); // 打印异常信息

      res.status(500).json({
        // 返回错误状态码和错误信息
        message: "短信发送失败，请稍后重试！",
      });
    }
  );

  // 设置验证码5分钟后过期
  const expiration = new Date();
  expiration.setMinutes(expiration.getMinutes() + 1);

  const puser = await PUserModel.findOne({ account: phoneNumber });

  if (!puser) {
    // 将用户信息保存到数据库
    const puser = new PUserModel({
      account: phoneNumber,
      resetCode: code.toString(),
      resetCodeExpires: expiration,
      state: false,
    });
    await puser.save();
  } else {
    puser.resetCode = code.toString();
    puser.resetCodeExpires = expiration;
    await puser.save();
  }
});

module.exports = router;
