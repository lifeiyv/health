var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var cors = require("cors");
// var expressJWT = require("express-jwt");
const { expressjwt: expressJWT } = require("express-jwt");

var WrjRouter = require("./routes/Wrj");
var JryRouter = require("./routes/Jry");
var FwcRouter = require("./routes/Fwc");
var ZwRouter = require("./routes/Zw");
var LfyRouter = require("./routes/Lfy");
var LmyRouter = require("./routes/Lmy");
var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(cors());
app.use("/upload", express.static("upload"));

app.use("/uploads", express.static(path.join(__dirname, "uploads")));

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/zw", ZwRouter);
app.use("/wrj", WrjRouter);
app.use("/jry", JryRouter);
app.use("/lfy", LfyRouter);
app.use("/lmy", LmyRouter);

app.use(
  expressJWT({ secret: "abcdefg", algorithms: ["HS256"] }).unless({
    path: [
      "/fwc/login",
      "/fwc/refresh",
      "/fwc/captcha",
      "/fwc/register",
      "/fwc/resetPassword",
      "/fwc/validateCode",
      "/fwc/confirmResetPassword",
      "/fwc/sendCode",
    ], // 跳过token验证
  })
);

// 配置中间件-错误捕获
app.use(function (err, req, res, next) {
  // token解析失败
  const authorization = req.headers["authorization"];
  if (!authorization) {
    return res.status(401).json({
      code: 20000,
      msg: "用户未登录",
    });
  }
  if (err.name === "UnauthorizedError") {
    return res.status(401).json({
      code: 20000,
      msg: "登录过期，请重新登录",
    });
  }
});

app.use("/fwc", FwcRouter);

// 不适合用于ap端，用在pc
// app.use(expressJWT.expressjwt({
//   secret:"lce",
//   algorithms:["HS256"]
// }).unless({
//   path:["/mtg/login",{url : /^\/upload/ , methods : ["GET"]}]
// }))

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

// 处理头像图片请求
app.get('/avatars/:filename', (req, res) => {
  const filename = req.params.filename;
  // 设置缓存头信息，例如设置缓存有效期为一周
  res.setHeader('Cache-Control', 'public, max-age=604800'); // 604800 秒 = 7 天
  res.sendFile(path.join(__dirname, 'public', 'avatars', filename));
});

// 启动服务器
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

module.exports = app;
